<?php


namespace app\admin\controller;
use think\Db;
use think\helper\Time;
/**
 * Class Stat 统计信息
 * @package app\admin\controller
 */
class Stat extends Admin
{
  public function getInfo()
  {
      // 今日开始和结束的时间戳
      $todaytime=Time::today();
      // 昨日开始和结束的时间戳
     $yesterday= Time::yesterday();
      // 本月开始和结束的时间戳
      $month=Time::month();
      // 上月开始和结束的时间戳
      $lastMonth=Time::lastMonth();
      $user['zcount']= Db("user")->count();//总人数
      $user['monthcount']= Db("user")->whereTime('create_time','between',[$this->formatTime($month[0]),$this->formatTime($month[1])])->count();//当月注册总人数
      $user['lastMonth']= Db("user")->whereTime('create_time','between',[$this->formatTime($lastMonth[0]),$this->formatTime($lastMonth[1])])->count();//上月注册总人数
      $user['todaycount']= Db("user")->whereTime('create_time','between',[$this->formatTime($todaytime[0]),$this->formatTime($todaytime[1])])->count();//当天注册总人数
      $user['yesterday']= Db("user")->whereTime('create_time','between',[$this->formatTime($yesterday[0]),$this->formatTime($yesterday[1])])->count();//昨天注册总人数
      $vodeo['zcount']= Db("video")->count();//总人数
      $vodeo['monthcount']= Db("video")->whereTime('create_time','between',[$this->formatTime($month[0]),$this->formatTime($month[1])])->count();//当月注册总人数
      $vodeo['lastMonth']= Db("video")->whereTime('create_time','between',[$this->formatTime($lastMonth[0]),$this->formatTime($lastMonth[1])])->count();//上月注册总人数
      $vodeo['todaycount']= Db("video")->whereTime('create_time','between',[$this->formatTime($todaytime[0]),$this->formatTime($todaytime[1])])->count();//当天注册总人数
      $vodeo['yesterday']= Db("video")->whereTime('create_time','between',[$this->formatTime($yesterday[0]),$this->formatTime($yesterday[1])])->count();//昨天注册总人数
      $text_image['zcount']= Db("text_image")->count();//总人数
      $text_image['monthcount']= Db("text_image")->whereTime('create_time','between',[$this->formatTime($month[0]),$this->formatTime($month[1])])->count();//当月注册总人数
      $text_image['lastMonth']= Db("text_image")->whereTime('create_time','between',[$this->formatTime($lastMonth[0]),$this->formatTime($lastMonth[1])])->count();//上月注册总人数
      $text_image['todaycount']= Db("text_image")->whereTime('create_time','between',[$this->formatTime($todaytime[0]),$this->formatTime($todaytime[1])])->count();//当天注册总人数
      $text_image['yesterday']= Db("text_image")->whereTime('create_time','between',[$this->formatTime($yesterday[0]),$this->formatTime($yesterday[1])])->count();//昨天注册总人数
    return success("成功",['user'=>$user,'video'=>$vodeo,'text_image'=>$text_image]);
  }

    public function formatTime($time)
    {
        return date("Y-m-d H:i:s",$time);
    }
}